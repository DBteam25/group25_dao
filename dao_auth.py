from werkzeug.security import generate_password_hash, check_password_hash
import mysql.connector as mysql
import datetime

class AuthHandler:
    def __init__(self,conn):
        self.conn = conn

    def new_user(self, name, surname, email, role_id, password):
        query = 'INSERT INTO user_accounts (name, surname, email, role_id, salt, password_hashed) VALUES (%s, %s, %s, %s, %s, %s);'
        hashed, salt = self.hash_and_salt(password)
        values = (name, surname, email, role_id, salt, hashed)
        cursor = self.conn.cursor()
        cursor.execute(query,values)
        self.conn.commit()

    def hash_and_salt(self, password):
        salt = str(datetime.datetime.now())
        # hashed = generate_password_hash(password)
        return (password, salt)

    def verify_password(self,password,hasht):

        return password == hasht

    def authenticate(self, email, password):
        query = 'SELECT password_hashed FROM user_accounts WHERE email = %s;'
        values = (email,)
        cursor = self.conn.cursor()
        cursor.execute(query,values)
        hasht = cursor.fetchone()[0]
        return self.verify_password(password,hasht)

if __name__ == '__main__':
    HOST = '192.168.99.100'
    DATABASE = 'database25'
    DATETIME_FORMAT = "%d-%b-%Y (%H:%M:%S.%f)"
    conn = mysql.connect(host = HOST, database = DATABASE, user = 'root', password = 'ppp')
    auth = AuthHandler(conn)
    # auth.new_user('Ryan','Ferner','ry@ry.ry',1,'password')
    print(auth.authenticate('ry@ry.ry','password'))
