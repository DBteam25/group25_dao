import json
import sseclient
import requests as req
import threading
import pprint
import queue
import time

class SSEHandler:
    '''Extend me and modify handle_data to decide what you want to do with data stream'''
    def __init__(self,url):
        self.start_stream(url)

    def start_stream(self,daurl):
        print(daurl)
        response = req.get(daurl, stream = True)
        self.client = sseclient.SSEClient(response)
        self.limit = 1 #set to 0 in production, thread will never stop

    def handle_data(self,data):
        #you need to override this in your extended version to decide what to do with the data
        pprint.pprint(data)
        url = 'http://dao-group-25.apps.dbgrads-6eec.openshiftworkshop.com/adddata'
        url = 'http://127.0.0.1:8080/adddata'
        data = {'data': json.dumps(data)}
        req.post(url, data = data)
   
    def start_absorbtion_thread(self):
        '''spawn a thread, it goes off and absorbs data and works with it forever'''
        thread = threading.Thread(target = self.start_absorbtion)
        # thread = threading.Thread(target = self.start_absorbtion, args = (...,), kwargs = {}) #you can use this if you want to pass in stuff
        thread.start()
        return thread #don't forget to kill me when i'm supposed to be finished

    def start_absorbtion(self):
        limit = self.limit
        for msg in self.client.events():
            data = json.loads(msg.data)
            self.handle_data(data)
            if not limit:
                break
            limit -= 1


if __name__ == '__main__':
    url = 'http://datagen-group-25.apps.dbgrads-6eec.openshiftworkshop.com/rawdeals'

    sse = SSEHandler(url)
    arr = []
    abs_thread = sse.start_absorbtion_thread()
    print('we get here, which means that we are threading')
    abs_thread.join()