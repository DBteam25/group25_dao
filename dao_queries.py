import mysql.connector as mysql
import json
from datetime import datetime
import numbers
import decimal

# HOST = "127.0.0.1"
HOST = "192.168.99.100"
DATABASE = "db_25"
DATETIME_FORMAT = "%d-%b-%Y (%H:%M:%S.%f)"


class sql_scripts:

    def get_instrument_id_by_name(self, conn, instrument_name):
        cursor = conn.cursor()
        query_for_instrument_id = "SELECT instrument_id " \
                                  "FROM instruments " \
                                  "WHERE instrument_name = %s"
        cursor.execute(query_for_instrument_id, (instrument_name,))
        try:
            ((result,),) = cursor.fetchall()
        except:
            raise ValueError()

        return result

    def get_counter_party_id_by_name(self, conn, counter_party_name):
        cursor = conn.cursor()
        while True:
            query_for_counter_party_id = "SELECT counter_party_id from counter_parties where counter_party_name = %s"
            cursor.execute(query_for_counter_party_id, (counter_party_name,))

            try:
                ((result,),) = cursor.fetchall()
                break
            except:
                query_for_new_counter_party = "insert into counter_parties (counter_party_name) values (%s)"
                cursor.execute(query_for_new_counter_party, (counter_party_name,))

        return result

    def get_type_id_by_name(self, conn, type_name):
        cursor = conn.cursor()
        query_for_type_id = "SELECT type_id from types where type_name = %s"
        cursor.execute(query_for_type_id, (type_name,))
        try:
            ((result,),) = cursor.fetchall()
        except:
            raise ValueError()

        return result

    def convert_cursor_results_to_json_format(self, row_headers, results):
        json_data = []
        for result in results:
            temp_result = []
            for field in result:
                if isinstance(field, numbers.Number):
                    field = float(field)
                if isinstance(field, datetime):
                    field = field.strftime(DATETIME_FORMAT)
                temp_result.append(field)
            json_data.append(dict(zip(row_headers, temp_result)))
        return json.dumps(json_data)

    def mask_for_select_query(self, cursor, query, values, multi=False):

        try:
            cursor.execute(query, values, multi=multi)
        except:
            return "Query Error"
        if cursor.description:
            row_headers = [x[0] for x in cursor.description]
            results = cursor.fetchall()
            cursor.close()
            return self.convert_cursor_results_to_json_format(row_headers, results)
        else:
            results = cursor.fetchall()
            print(results)
            return "no"

    def insert_deal_from_json(self, conn, json_object):
        cursor = conn.cursor()

        instrument_name = json_object['instrumentName']
        counter_party_name = json_object['cpty']
        price = json_object['__price']
        type_name = json_object['type']
        quantity = json_object['quantity']
        time = json_object['time']

        instrument_id = self.get_instrument_id_by_name(conn, instrument_name)
        counter_party_id = self.get_counter_party_id_by_name(conn, counter_party_name)
        type_id = self.get_type_id_by_name(conn, type_name)
        datetime_in_a_good_format = datetime.strptime(time, DATETIME_FORMAT)
        # print(datetime_in_a_good_format)

        query = "insert into deals (instrument_id, counter_party_id, price, type_id, quantity, time) " \
                "values(%s, %s, %s, %s, %s, %s)"
        values = (instrument_id, counter_party_id, price, type_id, quantity, datetime_in_a_good_format)

        # if True:
        try:
            cursor.execute(query, values)
            conn.commit()
        except:
            conn.rollback()
            # print("errorrrr")

        cursor.close()

    def insert_deals_to_mysql_database_from_json(self, conn, json_file):
        with open(json_file) as json_file:
            data = json.load(json_file)
            for one_object in data:
                self.insert_deal_from_json(conn, one_object)

    def select_deals_from_time_interval(self, conn, start, end):
        cursor = conn.cursor()
        query = "SELECT r.deal_id, l.instrument_name, c.counter_party_name, r.price, t.type_name, " \
                "r.quantity, r.time FROM instruments l LEFT JOIN deals r using(instrument_id) LEFT JOIN " \
                "types t using(type_id) LEFT JOIN counter_parties c using(counter_party_id) " \
                "where r.time >= %s and r.time <= %s;"
        values = (start, end)
        return self.mask_for_select_query(cursor, query, values)

    # User requirement 7
    def select_average_price_for_each_instrument(self, conn, type_name):
        cursor = conn.cursor()
        if type_name not in ('B', 'S'):
            raise ValueError()

        query = "SELECT l.instrument_name, AVG(r.price) as average_price " \
                "FROM instruments l " \
                "LEFT JOIN deals r using(instrument_id) " \
                "LEFT JOIN types t using(type_id) " \
                "WHERE t.type_name = %s " \
                "GROUP BY l.instrument_name;"
        values = (type_name,)

        return self.mask_for_select_query(cursor, query, values)

    def select_average_price_for_each_instrument_for_time_interval(self, conn, type_name, start, end):
        cursor = conn.cursor()
        if type_name not in ('B', 'S'):
            raise ValueError()

        query = "SELECT l.instrument_name, AVG(r.price) as average_price " \
                "FROM instruments l " \
                "LEFT JOIN deals r using(instrument_id) " \
                "LEFT JOIN types t using(type_id) " \
                "WHERE t.type_name = %s AND " \
                "r.time >= %s AND r.time <= %s " \
                "GROUP BY l.instrument_name;"
        values = (type_name, start, end)

        return self.mask_for_select_query(cursor, query, values)

    def select_profit_loss_for_each_counter_party(self, conn):
        cursor = conn.cursor()
        query = '''CREATE OR REPLACE VIEW profit AS 
                SELECT counter_party_id, sum(price * quantity) as profit 
                FROM deals where type_id = 1 GROUP BY counter_party_id; '''
        cursor.execute(query)
        query = ''' CREATE OR REPLACE VIEW loss AS SELECT counter_party_id, 
                sum(price * quantity) as loss FROM 
                deals where type_id = 2 GROUP BY counter_party_id; '''
        cursor.execute(query)
        query = '''SELECT cp.counter_party_name, (p.profit - l.loss ) 
                FROM loss as l, profit as p, counter_parties as cp WHERE 
                l.counter_party_id = p.counter_party_id  AND l.counter_party_id = cp.counter_party_id;'''
        values = ()
        return self.mask_for_select_query(cursor, query, values, multi=True)

    def select_all_data(self, conn):
        cursor = conn.cursor()
        query = "SELECT r.deal_id, l.instrument_name, c.counter_party_name, r.price, t.type_name, " \
                "r.quantity, r.time FROM instruments l LEFT JOIN deals r using(instrument_id) LEFT JOIN " \
                "types t using(type_id) LEFT JOIN counter_parties c using(counter_party_id);"

        values = ()

        return self.mask_for_select_query(cursor, query, values)


    def get_effective_profit(self, conn):
        cursor = conn.cursor()
        query = '''create or replace view last_price 
                as ( Select instrument_id, price
                from deals d1
                where time = (	select max(time)
                                from deals d2
                                where d1.instrument_id = d2.instrument_id)
                    ); '''
        cursor.execute(query)

        query = ''' CREATE OR REPLACE VIEW items_sold
                    AS ( SELECT counter_party_id, instrument_id, sum(quantity) as quantity_items_sold, sum(quantity * price) as revenue
                    FROM deals 
                    where type_id = 1 
                    GROUP BY counter_party_id, instrument_id
                    ); '''

        cursor.execute(query)
        query = ''' CREATE OR REPLACE VIEW items_bought
                    AS (	SELECT counter_party_id, instrument_id, sum(quantity) as quantity_items_bought, avg(price) as avg_price 
                    FROM deals 
                    where type_id = 2 
                    GROUP BY counter_party_id, instrument_id
                    ); '''
        cursor.execute(query)
        query = ''' CREATE OR REPLACE VIEW realized_profit
                    AS (   SELECT s.counter_party_id, s.instrument_id, 
                    (s.revenue - (b.quantity_items_bought * b.avg_price)) as realized_profit, 
                            b.quantity_items_bought - s.quantity_items_sold as inventory
                            FROM items_sold as s, items_bought as b
                            WHERE s.counter_party_id = b.counter_party_id  
                            AND s.instrument_id = b.instrument_id
                            ); '''
        cursor.execute(query)
        query = ''' select rp.counter_party_id, rp.instrument_id, (rp.realized_profit + (rp.inventory * lp.price)) as effective_profit
                    from realized_profit as rp, last_price as lp
                    where rp.instrument_id = lp.instrument_id;
                '''

        values = ()

        return self.mask_for_select_query(cursor, query, values)

    

if __name__ == '__main__':
    queries = sql_scripts()
    HOST = '192.168.99.100'
    DATABASE = 'database25'
    conn = mysql.connect(host = HOST, database = DATABASE, user = 'root', password = 'ppp')
    print (queries.select_profit_loss_for_each_counter_party(conn))
