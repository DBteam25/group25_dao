from flask import Flask, Response, request
from flask_cors import CORS
from sse_absorber import SSEHandler
from dao_queries import sql_scripts
from dao_auth import AuthHandler
import mysql.connector as mysql
import queue, os, json

class DaoSSEHandler(SSEHandler):
    def __init__(self,url,conn):
        self.start_stream(url)
        self.conn = conn
    def handle_data(self,data):
        scripts = sql_scripts()
        # print(data)
        scripts.insert_deal_from_json(self.conn, data)

app = Flask(__name__)
CORS(app)


@app.route('/checkconn')
def checkConn():
    if conn:
        return 'connected to database'
    else:
        return 'database connection error'

@app.route('/')
def index():
    return 'hello world'

@app.route('/alldata', methods = ['GET'])
def all_data():
    return query.select_all_data(conn)

@app.route('/instrumentbyname', methods = ['GET'])
def instrument_by_name():
    name = request.args.get('name')
    return str(query.get_instrument_id_by_name(conn,name))

@app.route('/counterpartybyname', methods = ['GET'])
def counterparty_by_name():
    name = request.args.get('name')
    return str(query.get_counter_party_id_by_name(conn,name))

@app.route('/typeidbyname', methods = ['GET'])
def type_id_by_name():
    name = request.args.get('name')
    return str(query.get_type_id_by_name(conn,name))

@app.route('/dealsintimeinterval', methods = ['GET'])
def deals_interval():
    start = request.args.get('start')
    end = request.args.get('end')
    # DATETIME_FORMAT = "%d-%b-%Y (%H:%M:%S.%f)"
    #maybe i should reformat this later, maybe not
    return query.select_deals_from_time_interval(conn,start,end)

@app.route('/avginstrumenttypeprice', methods = ['GET'])
def avg_instrument_price():
    name = request.args.get('type')
    #maybe i should reformat this later, maybe not
    return query.select_average_price_for_each_instrument(conn,name)

@app.route('/avginstrumenttypepriceinterval', methods = ['GET'])
def avg_instrument_price_over_interval():
    start = request.args.get('start')
    end = request.args.get('end')
    name = request.args.get('type')
    # DATETIME_FORMAT = "%d-%b-%Y (%H:%M:%S.%f)"
    #maybe i should reformat this later, maybe not
    return query.select_average_price_for_each_instrument_for_time_interval(conn,name,start,end)

@app.route('/profitloss', methods = ['GET']) #I have no idea why, but this is super broken.
def profit_loss():
    return query.select_profit_loss_for_each_counter_party(conn)

@app.route('/newuser', methods = ['POST'])
def new_user():
    name = request.form.get('name')
    surname = request.form.get('surname')
    email = request.form.get('email')
    role_id = 0
    password = request.form.get('password')
    auth.new_user(name,surname,email,role_id,password)
    return 'User Added!'

@app.route('/login', methods = ['GET'])
def login():
    email = request.args.get('email')
    password = request.args.get('password')
    if auth.authenticate(email,password):
        return 'login successful'
    else:
        return 'login failed'

@app.route('/adddata', methods = ['POST'])
def addData():
    data = request.values.get('data')
    print(data)
    data = json.loads(data)
    
    query.insert_deal_from_json(conn, data)
    return "success"


if __name__ == '__main__':
    HOST = '192.168.99.100'
    DATABASE = 'database25'
    DATETIME_FORMAT = "%d-%b-%Y (%H:%M:%S.%f)"
    URL = 'http://datagen-group-25.apps.dbgrads-6eec.openshiftworkshop.com/rawdeals'
    PASSWORD = 'ppp'
    try:
        HOST = os.environ['DBLOCATION']
        URL =  os.environ['STREAMURL']
        PASSWORD = os.environ['DBPASSWORD']
    except:
        pass
    
    conn = None
    query = sql_scripts()
    try:
        conn = mysql.connect(host = HOST, database = DATABASE, user = 'root', password = PASSWORD)
        sseconn = mysql.connect(host = HOST, database = DATABASE, user = 'root', password = PASSWORD)
        authconn = mysql.connect(host = HOST, database = DATABASE, user = 'root', password = PASSWORD)
        auth = AuthHandler(authconn)
    except:
        print('Connection to DAO failed')
    try:
        sse = DaoSSEHandler(URL,sseconn)
        sse.start_absorbtion_thread()
        

    except Exception as exc:
        print('Datagen connection failed')
        print(exc)


    # when dockerizing this, uncomment the following line
    app.run(port=8080, debug = True, threaded=True, host=('0.0.0.0'))

    # when dockerizing this, comment the following line
    # app.run(port=8080, threaded=True)
